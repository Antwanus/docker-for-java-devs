PS C:\Users\veree> docker network ls
    NETWORK ID     NAME                           DRIVER    SCOPE
    9a949e156b0c   bridge                         bridge    local
    e0af3cdeab90   docker_default                 bridge    local
    0a317048350a   host                           host      local
    7b9c7a6d4dae   mein-network                   bridge    local
    3f2311284a94   mysql-adminer_default          bridge    local
    c5902e01676f   none                           null      local
    5507e6b11417   vdm-springbackend-v2_default   bridge    local
    PS C:\Users\veree> docker network prune
    WARNING! This will remove all custom networks not used by at least one container.
    Are you sure you want to continue? [y/N] y
    Deleted Networks:
    mein-network
    docker_default
    mysql-adminer_default
    vdm-springbackend-v2_default

PS C:\Users\veree> docker network ls
    NETWORK ID     NAME      DRIVER    SCOPE
    9a949e156b0c   bridge    bridge    local
    0a317048350a   host      host      local
    c5902e01676f   none      null      local

PS C:\Users\veree> docker network create mein_network
    676b3bdd0629face075af7e466c326d4ec7e4d412e5030af561df6575d23be20

PS C:\Users\veree> docker images
    REPOSITORY                  TAG                     IMAGE ID       CREATED         SIZE
    4ntoon/fleetman-webapp      0.0.1-SNAPSHOT          a2ce7b848f62   13 hours ago    175MB
    4ntoon/fleetman-webapp      latest                  a2ce7b848f62   13 hours ago    175MB
    mysql                       5                       2c9028880e58   5 days ago      447MB

PS C:\Users\veree> docker container run -d --name mein_mysql --volume mein_data:/var/lib/mysql --network mein_network -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=fleetman mysql:5
    c6f6767595baea2801f7bb6c1187fd748aa742da917c9da7b99fcb27e42ab34f

PS C:\Users\veree> docker logs mein_msql
    ....

PS C:\Users\veree> docker container run -d --network mein_network --name fleetman-webapp -p 80:8080 4ntoon/fleetman-webapp
    a700058fc7d2bb4d41704ec24876c3cb7d52c4d7ce016b809d2fc141bd8e2856

        !!! keep in mind u need to rebuild a jar, image & publish to dockerhub if you made changes !!!
PS C:\Users\veree> docker container run --network mein_network --name fleetman-webapp -p 80:8080 4ntoon/fleetman-webapp
      .   ____          _            __ _ _
     /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
    ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
     \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
      '  |____| .__|_| |_|_| |_\__, | / / / /
     =========|_|==============|___/=/_/_/_/
     :: Spring Boot ::        (v2.3.0.RELEASE)

    2021-05-17 12:29:41.696  INFO 1 --- [           main] c.c.c.ConfigServicePropertySourceLocator : Fetching config from server at : http://localhost:8888
    2021-05-17 12:29:41.794  INFO 1 --- [           main] c.c.c.ConfigServicePropertySourceLocator : Connect Timeout Exception on Url - http://localhost:8888. Will be trying the next url if available
    2021-05-17 12:29:41.795  WARN 1 --- [           main] c.c.c.ConfigServicePropertySourceLocator : Could not locate PropertySource: I/O error on GET request for "http://localhost:8888/application/docker-demo": Connection refused (Connection refused); nested exception is java.net.ConnectException: Connection refused (Connection refused)
    2021-05-17 12:29:41.797  INFO 1 --- [           main] c.v.TomcatWARApplication                 : The following profiles are active: docker-demo
    2021-05-17 12:29:42.791  INFO 1 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Bootstrapping Spring Data JPA repositories in DEFERRED mode.
    2021-05-17 12:29:42.972  INFO 1 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Finished Spring Data repository scanning in 174ms. Found 1 JPA repository interfaces.
    2021-05-17 12:29:43.191  INFO 1 --- [           main] o.s.cloud.context.scope.GenericScope     : BeanFactory id=cb6b3929-bbcf-3865-be8a-46fa8bf31e4d
    2021-05-17 12:29:43.684  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
    2021-05-17 12:29:43.697  INFO 1 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
    2021-05-17 12:29:43.697  INFO 1 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.35]
    2021-05-17 12:29:44.028  INFO 1 --- [           main] org.apache.jasper.servlet.TldScanner     : At least one JAR was scanned for TLDs yet contained no TLDs. Enable debug logging for this logger for a complete list of JARs that were scanned but no TLDs were found in them. Skipping unneeded JARs during scanning can improve startup time and JSP compilation time.
    2021-05-17 12:29:44.172  INFO 1 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
    2021-05-17 12:29:44.172  INFO 1 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 2352 ms
    2021-05-17 12:29:44.507  INFO 1 --- [           main] o.hibernate.jpa.internal.util.LogHelper  : HHH000204: Processing PersistenceUnitInfo [name: default]
    2021-05-17 12:29:44.853  INFO 1 --- [           main] org.hibernate.Version                    : HHH000412: Hibernate ORM core version 5.4.15.Final
    2021-05-17 12:29:44.999  INFO 1 --- [           main] o.hibernate.annotations.common.Version   : HCANN000001: Hibernate Commons Annotations {5.1.0.Final}
    2021-05-17 12:29:45.184  INFO 1 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Starting...
    2021-05-17 12:29:45.360  INFO 1 --- [           main] com.zaxxer.hikari.pool.PoolBase          : HikariPool-1 - Driver does not support get/set network timeout for connections. (com.mysql.jdbc.JDBC4Connection.getNetworkTimeout()I)
    2021-05-17 12:29:45.363  INFO 1 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Start completed.
    2021-05-17 12:29:45.423  INFO 1 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.MySQL57Dialect
    2021-05-17 12:29:46.169  INFO 1 --- [           main] o.h.e.t.j.p.i.JtaPlatformInitiator       : HHH000490: Using JtaPlatform implementation: [org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform]
    2021-05-17 12:29:46.179  INFO 1 --- [           main] j.LocalContainerEntityManagerFactoryBean : Initialized JPA EntityManagerFactory for persistence unit 'default'
    2021-05-17 12:29:46.256  INFO 1 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'clientInboundChannelExecutor'
    2021-05-17 12:29:46.259  INFO 1 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'clientOutboundChannelExecutor'
    2021-05-17 12:29:46.905  INFO 1 --- [           main] com.virtualpairprogrammers.Startup       : Creating record for city_truck
    2021-05-17 12:29:46.964  INFO 1 --- [           main] com.virtualpairprogrammers.Startup       : Creating record for village_truck
    2021-05-17 12:29:46.985  INFO 1 --- [           main] o.s.s.c.ThreadPoolTaskScheduler          : Initializing ExecutorService 'messageBrokerTaskScheduler'
    2021-05-17 12:29:47.028  INFO 1 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'brokerChannelExecutor'
    2021-05-17 12:29:47.056  WARN 1 --- [           main] JpaBaseConfiguration$JpaWebConfiguration : spring.jpa.open-in-view is enabled by default. Therefore, database queries may be performed during view rendering. Explicitly configure spring.jpa.open-in-view to disable this warning
    2021-05-17 12:29:47.102  WARN 1 --- [           main] o.s.c.n.a.ArchaiusAutoConfiguration      : No spring.application.name found, defaulting to 'application'
    2021-05-17 12:29:47.107  WARN 1 --- [           main] c.n.c.sources.URLConfigurationSource     : No URLs will be polled as dynamic configuration sources.
    2021-05-17 12:29:47.107  INFO 1 --- [           main] c.n.c.sources.URLConfigurationSource     : To enable URLs as dynamic configuration sources, define System property archaius.configurationSource.additionalUrls or make config.properties available on classpath.
    2021-05-17 12:29:47.114  WARN 1 --- [           main] c.n.c.sources.URLConfigurationSource     : No URLs will be polled as dynamic configuration sources.
    2021-05-17 12:29:47.115  INFO 1 --- [           main] c.n.c.sources.URLConfigurationSource     : To enable URLs as dynamic configuration sources, define System property archaius.configurationSource.additionalUrls or make config.properties available on classpath.
    2021-05-17 12:29:48.003  WARN 1 --- [           main] ockingLoadBalancerClientRibbonWarnLogger : You already have RibbonLoadBalancerClient on your classpath. It will be used by default. As Spring Cloud Ribbon is in maintenance mode. We recommend switching to BlockingLoadBalancerClient instead. In order to use it, set the value of `spring.cloud.loadbalancer.ribbon.enabled` to `false` or remove spring-cloud-starter-netflix-ribbon from your project.
    2021-05-17 12:29:48.089  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
    2021-05-17 12:29:48.091  INFO 1 --- [           main] o.s.m.s.b.SimpleBrokerMessageHandler     : Starting...
    2021-05-17 12:29:48.092  INFO 1 --- [           main] o.s.m.s.b.SimpleBrokerMessageHandler     : BrokerAvailabilityEvent[available=true, SimpleBrokerMessageHandler [DefaultSubscriptionRegistry[cache[0 destination(s)], registry[0 sessions]]]]
    2021-05-17 12:29:48.092  INFO 1 --- [           main] o.s.m.s.b.SimpleBrokerMessageHandler     : Started.
    2021-05-17 12:29:48.094  INFO 1 --- [           main] DeferredRepositoryInitializationListener : Triggering deferred initialization of Spring Data repositories…
    2021-05-17 12:29:48.095  INFO 1 --- [           main] DeferredRepositoryInitializationListener : Spring Data repositories initialized!
    2021-05-17 12:29:48.108  INFO 1 --- [           main] c.v.TomcatWARApplication                 : Started TomcatWARApplication in 7.161 seconds (JVM running for 7.581)




